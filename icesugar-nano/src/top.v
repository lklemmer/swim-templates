module top
    ( input clk
    , output led
    );

    reg rst = 1;
    always @(posedge clk) begin
        rst <= 0;
    end

    \proj::main::main main
        ( .clk_i(clk)
        , .rst_i(rst)
        , .output__(led)
        );
endmodule
